#include <Keyboard.h>

#define		sensorPin		9

bool triggered = false;

void setup(){
    Serial.begin( 9600 );
	pinMode( sensorPin, INPUT_PULLUP );
	attachInterrupt(	digitalPinToInterrupt( sensorPin ),
						sensorTripped,
						CHANGE );

    Serial.println( "Set up" );
}

void loop(){
	if( triggered ){
        Serial.println( "Triggered" );
		changeScreens();
		
		triggered = false;
	}
}

void changeScreens(){
	Keyboard.begin();
	
	for( int i = 1; i < 20; i++ ){
			Keyboard.press( KEY_LEFT_CTRL );
			Keyboard.press( KEY_LEFT_GUI );
			Keyboard.press( KEY_LEFT_ARROW );
			Keyboard.releaseAll();
			delay( 10 );
	}
		
	Keyboard.end();
}

void sensorTripped(){
	triggered = true;
}
