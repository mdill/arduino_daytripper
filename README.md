# Arduino Fish Tank Light Controller

## Purpose

This is a pair of simple sketches designed to control your Windows desktop, via
TaskView, by deploying two Arduino boards.

One sketch goes on a wireless device with a laser trip-wire to detect an
approaching "visitor" before they reach your office.  This device is our
transmitter.

The second sketch plugs into your computer via USB to emulate a keyboard, and is
our receiver.  Once a signal is received from the transmitter, the receiver
sends multiple instances of the key-combination "Ctrl+Win+Left" to automatically
send you to the leftmost desktop in TaskView.

As long as you have "productive" applications open in the leftmost desktop, your
"visitor" will never know you were on another workspace slacking off.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/arduino_dayTripper.git

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/arduino_daytripper/src/master/LICENSE.txt) file for
details.

